const express = require('express');
const jwt = require('jsonwebtoken');

const router = express.Router();
const User = require('../Models/User');

router.get('/test', (req, res, next) => {
    try {
        res.status(200).send({
            success: true,
            message: 'Testing api response from auth'
        });
    } catch (e) {
        res.status(200).send({
            success: true,
            message: e.message
        });
    }
});

router.post('/login', (req, res, next) => {
    User.findOne({
        email: req.body.email
    }, (err, user) => {
        try {
            if (err) throw new Error('Critical error: '.err);
            if (!user) {
                throw new Error('Authentication failed. User not found.');
            } else if (user) {
                // check if password matches
                if (user.password !== req.body.password) {
                    throw new Error('Authentication failed. Wrong password.');
                } else {
                    // if user is found and password is right
                    // create a token with only our given payload
                    // we don't want to pass in the entire user since that has the password
                    const payload = { id: user.id };
                    const token = jwt.sign(payload, req.app.get('superSecret'), {
                        expiresIn: 1440 // expires in 24 hours
                    });
                    res.status(200).send({
                        success: true,
                        message: 'Enjoy your token!',
                        token
                    });
                }
            }
        } catch (e) {
            res.status(200).send({
                success: false,
                message: e.message
            });
        }
    });
});


module.exports = router;
