/**
 * Created by sahid
 */
const mongooes = require('mongoose');

const userSchema = mongooes.Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String
    },
    email: {
        type: String,
        required: true,
        index: { unique: true }
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        default: 'user'
    }
});
const User = mongooes.model('User', userSchema);
module.exports = User;
