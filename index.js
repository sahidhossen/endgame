
/**
 * Module dependencies.
 */
const chalk = require('chalk');

/**
 * Load configuration
 */
const config = require('./config');

/**
 * Load all app settings from server
 */
const app = require('./server');

/**
 * Start Express server.
 */
app.listen(config.server.port, () => {
    console.log('%s App is running at http://localhost:%s in %s mode', chalk.green('✓'), config.server.port, config.env);
    console.log('  Press CTRL-C to stop\n');
});
