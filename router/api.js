const express = require('express');

const router = express.Router();

const Auth = require('../App/Service/Authentication');
const checkAuth = require('../App/Service/checkAuth');
const Users = require('../App/Controllers/Users');

// For user registration
router.use('/', Auth);

/**
 * Authentication middleware
 * Check oauthentication in each request
 */
router.use(checkAuth);

router.use('/user', Users);

module.exports = router;
