/**
 * All of the route will configure from here
 * API | WEB
 */

const express = require('express');

const routes = express.Router();
const API = require('./api');

routes.use('/api/v1', API);

module.exports = routes;
