
/**
 * Module dependencies.
 */
const express = require('express');
const helmet = require('helmet');
const chalk = require('chalk');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const expressStatusMonitor = require('express-status-monitor');

// Require all routes
const router = require('../router');

/**
 * Apply middlware for security purpose
 */
const app = express();
app.set('superSecret', process.env.APP_KEY); // secret variable

/**
 * Connect to MongoDB.
 */
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.connect(process.env.MONGODB_URI);
mongoose.connection
    .on('open', () => {
        console.log('%s MongoDB connection success.', chalk.green('✓'));
    })
    .on('error', (err) => {
        console.error(err);
        console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
        process.exit();
    });

/**
 * User Helmet for help protect your app from some well-known web
 * vulnerabilities by setting HTTP headers appropriately
 * More Info: https://expressjs.com/en/advanced/best-practice-security.html#use-tls
 */
app.use(helmet());

app.use(expressStatusMonitor());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use('/', router);

module.exports = app;
