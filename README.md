# The End Game

This project will handle all of the api request from other device, browser or any where we want to place our service.

### Dev Dependency 
----------------------------
**ENVIRONMENT VARIABLES**

Please rename the env.example file to .env file. It will add all configure to node process.env. 
Update .env file if necessary. It will not update in git repository. 

**MONGODB**

We are using [Mongodb](https://www.mongodb.com/) for our database system. So please first install the mongodb and create a database called `endgame` and update the .env file


### Getting Started
----------------------------
```
git clone https://sahidhossen@bitbucket.org/sahidhossen/endgame.git

cd directory

npm install

npm run dev
```

It will open your project in your [localhost:3000](http://localhost:3000). This url will be empty because of json response. 

_You can find our api end point-_
```
http://localhost:3000/api/v1/test
```

### Reports real time server metrics
----------------------------
We have add real-time server metrics for monitor our project. You can check the report from [localhost:3000/status](http://localhost:3000/status)


### Code check with Eslint
----------------------------
In every time when we want to push on server please check the code if its satisfy the `eslint` condition.

```
npm run lint 

Or 

npm run lint-fix
```

### Code test with Mocha
----------------------------
Please test your code and check the tester file from `./nyc_output` folder form your project folder. It will tell you the states of your code.

```
npm run test 

```